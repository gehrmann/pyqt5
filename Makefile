#SIP_VERSION=			4.15.5

#SIP_VERSION=			4.18
#QT_VERSION=				5.7

#SIP_VERSION=			4.19.4
##SIP_VERSION=			4.19.6
#QT_VERSION=				5.9.2

SIP_VERSION=			4.19.12
QT_VERSION=				5.11.2

#SIP_VERSION=			4.19.13
#QT_VERSION=				5.11.3

# Basic libraries
TARGETS+=Qt.so
TARGETS+=QtCore.so
TARGETS+=QtGui.so
TARGETS+=QtWidgets.so
TARGETS+=pyrcc.so

## Full libraries
#TARGETS+=Qt.so
#TARGETS+=QtBluetooth.so
#TARGETS+=QtCore.so
#linux linux-i86_64: TARGETS+=QtDBus.so
#linux linux-i86_64: TARGETS+=QtDesigner.so
#TARGETS+=QtGui.so
#TARGETS+=QtHelp.so
#TARGETS+=QtLocation.so
#TARGETS+=QtMultimedia.so
#TARGETS+=QtNetwork.so
#TARGETS+=QtNfc.so
#TARGETS+=QtOpenGL.so
#TARGETS+=QtPositioning.so
#TARGETS+=QtPrintSupport.so
#TARGETS+=QtQml.so
#TARGETS+=QtQuick.so
#TARGETS+=QtQuickWidgets.so
#TARGETS+=QtSensors.so
#TARGETS+=QtSerialPort.so
#TARGETS+=QtSql.so
#TARGETS+=QtSvg.so
#TARGETS+=QtTest.so
#TARGETS+=QtWebChannel.so
##TARGETS+=QtWebKit.so
###TARGETS+=QtWebKitWidgets.so
#linux linux-i86_64: TARGETS+=QtWebEngine.so
#linux linux-i86_64: TARGETS+=QtWebEngineCore.so
#linux linux-i86_64: TARGETS+=QtWebEngineWidgets.so
#TARGETS+=QtWebSockets.so
#TARGETS+=QtWidgets.so
#linux linux-i86_64: TARGETS+=QtX11Extras.so
#TARGETS+=QtXml.so
#TARGETS+=QtXmlPatterns.so

#linux linux-i86_64: TARGETS+=_QOpenGLFunctions_2_0.so
#linux linux-i86_64: TARGETS+=_QOpenGLFunctions_2_1.so
#linux linux-i86_64: TARGETS+=_QOpenGLFunctions_4_1_Core.so

# Linux
linux linux-i86_64: OS=linux
linux linux-i86_64: ARCHITECTURE=x86_64
linux linux-i86_64: build

# Android
android android-arm: OS=android
android android-arm: ARCHITECTURE=arm
android android-arm: build

build:
	OS=${OS} ARCHITECTURE=${ARCHITECTURE} SIP_VERSION=${SIP_VERSION} QT_VERSION=${QT_VERSION} \
		make -f Makefile-${OS} \
			$(addprefix .build/.${OS}-${ARCHITECTURE}-${QT_VERSION}/,${TARGETS})
